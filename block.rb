require "gitlab"
require "uri"
require "net/http"
require "net/https"
require "json"

private_token = ENV["PRIVATE_TOKEN"]
zapier_webhook = ENV.fetch("ZAPIER_WEBHOOK", "https://hooks.zapier.com/hooks/catch/632410/o523sam")

endpoint = ENV.fetch("ENDPOINT_URL", "https://gitlab.lyle.work/api/v4")
user = ENV.fetch("USER","lkozloff-admin")
action = ENV.fetch("ACTION","unblock")
reason = ENV.fetch("REASON","https://gitlab.zendesk.com/ticket/100156")
ci_pipeline_url = ENV.fetch("CI_PIPELINE_URL","couldn't get URL")
requesting_user = ENV.fetch("GITLAB_USER_LOGIN", "couldn't get requesting user")

g = Gitlab.client(
  endpoint: endpoint,
  private_token: private_token )

user = g.user_search(user).first
@toSend = {
  "time" => DateTime.now, 
  "action" => action, 
  "user" => user.username, 
  "reason" => reason,
  "ci_pipeline_url" => ci_pipeline_url,
  "requesting_user" => requesting_user
}.to_json

uri = URI.parse(zapier_webhook)
https = Net::HTTP.new(uri.host,uri.port)
https.use_ssl = true
req = Net::HTTP::Post.new(uri.path, initheader = {'Content-Type' =>'application/json'})
req['foo'] = 'bar'
req.body = "[ #{@toSend} ]"

#do the action
if action == "block"
   g.block_user(user.id)
elsif action == "unblock"
   g.unblock_user(user.id)
end

#record it
puts "%s - %sing %s for work on %s" % [DateTime.now, action, user.username, reason]
res = https.request(req)
puts "Response #{res.code} #{res.message}: #{res.body}"
